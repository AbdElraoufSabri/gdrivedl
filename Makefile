destinationDir=/opt/gdrivedl
THIS_FILE := $(lastword $(MAKEFILE_LIST))

all: install

install:
	@echo -n "Installing gdrivedl .. " ||:
	@sudo mkdir -p $(destinationDir) ||:
	@sudo cp -r . $(destinationDir) ||:
	@sudo ln -sf $(destinationDir)/gdrivedl /usr/local/bin/gdrivedl ||:
	@sudo ln -sf $(destinationDir)/UPDATE /usr/local/bin/gdrivedl-update ||:
	@sudo ln -sf $(destinationDir)/UNINSTALL /usr/local/bin/gdrivedl-uninstall ||:
	@sudo chmod +x $(destinationDir)/gdrivedl ||:
	@sudo chmod +x $(destinationDir)/UPDATE ||:
	@sudo chmod +x $(destinationDir)/UNINSTALL ||:
	@echo "Done" ||:
	@echo
	@echo "Installation summary" ||:
	@echo '╔═════════════════════╦════════════════════════════════════════════════════════╗' ||:
	@echo '║       Command       ║                         usage                          ║' ||:
	@echo '╠═════════════════════╬════════════════════════════════════════════════════════╣' ||:
	@echo '║ gdrivedl            ║ gdrivedl application                                   ║' ||:
	@echo '║ gdrivedl-update     ║ Updates gdrivedl to latest version from the repository ║' ||:
	@echo '║ gdrivedl-uninstall  ║ Uninstalls gdrivedl completely                         ║' ||:
	@echo '╚═════════════════════╩════════════════════════════════════════════════════════╝' ||:

uninstall:
	@echo -n "Uninstalling gdrivedl .. " ||:
	@sudo rm $(destinationDir)/* -rf ||:
	@sudo rm /usr/local/bin/gdrivedl ||:
	@sudo rm /usr/local/bin/gdrivedl-update ||:
	@sudo rm /usr/local/bin/gdrivedl-uninstall ||:
	@echo "Done" ||:

update:
	@echo "Updating gdrivedl:" ||:
	@echo -n "    " ||:
	@$(MAKE) -f $(THIS_FILE) uninstall --no-print-directory
	@echo -n "    " ||:
	@$(MAKE) -f $(THIS_FILE) install --no-print-directory
	@echo "Done" ||:
